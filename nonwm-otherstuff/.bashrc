#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias clnarch="sudo pacman -Qtdq && sudo pacman -Rns $(pacman -Qtdq)"
alias wifi="nmcli device wifi list"
alias up="nmcli --ask device wifi connect"
alias less='less --RAW-CONTROL-CHARS'
export LS_OPTS='--color=always'
export LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33'
alias ls='ls ${LS_OPTS}'

export PS1="\[$(tput setaf 161)\]\u\[$(tput setaf 112)\]@\h\[$(tput setaf 011)\]\W~$> \[$(tput sgr 0)\]"

fortune | cowsay | lolcat --spread 5.0 --freq 0.2  

